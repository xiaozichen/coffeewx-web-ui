import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: '/api/wx/subscribe/text/list',
    method: 'post',
    params:data
  })
}

export function createRow(data) {
  return request({
    url: '/api/wx/subscribe/text/add',
    method: 'post',
    data
  })
}

export function updateRow(data) {
  return request({
    url: '/api/wx/subscribe/text/update',
    method: 'post',
    data
  })
}
export function deleteRow(data) {
  return request({
    url: '/api/wx/subscribe/text/delete',
    method: 'post',
    params:data
  })
}

